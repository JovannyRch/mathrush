import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router'

@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss'],
})
export class LanguagePage implements OnInit {

  constructor(private storage: Storage, private router: Router) { }


  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: null
  };

  labelActual: number = 1;

  ngOnInit() {

    this.storage.get('conf').then((val) => {
      if (val) this.conf = val;
      else this.storage.set('conf', this.conf);
    });

    setInterval(() => {
      if(this.labelActual == 1) this.labelActual = 2;
      else this.labelActual = 1;
    }, 1600);

  }

  setIdioma(idioma: number){
    this.conf.idioma = idioma;
    this.storage.set('conf', this.conf);
    
    this.router.navigate(["/menu1"]);

  } 

}
