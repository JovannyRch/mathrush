import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.page.html',
  styleUrls: ['./menu1.page.scss'],
})
export class Menu1Page implements OnInit {


  //Dificultad
  dificultades: any = [
    {id: 0,label: {1: 'Fácil', 2: "Easy"}},
    {id: 1,label: {1: 'Normal', 2: "Normal"}},
    {id: 2,label: {1: 'Difícil', 2: "Hard"}},
  ]

  conf:any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: 1
  };


   //Tipo de tiempo
   tiempos: any = [
    {id: 0,label: {1: '1 Minuto', 2: "1 Minute"}},
    {id: 1,label:  {1: '2 Minutos', 2: "2 Minutes"}},
    {id: 2,label:  {1: '3 Minutos', 2: "3 Minutes"}},
   // {id: 2,label: 'Sin tiempo '}
  ];
  

  // Modos de juego

  modos: any = [

    {id: 5, label: {1: "Todos los modos", 2:"All modes"}, img: "assets/img/5.png"},
    {id: 6, label: {1: "Suma y Resta", 2:"Addition and Subtraction"}, img: "assets/img/6.png"},
    {id: 7, label: {1: "Multiplicación y División", 2:"Multiplication and Division"}, img: "assets/img/7.png"},
    {id: 1, label: {1:"Suma",2:"Addition"}, img: "assets/img/1.png"},
    {id: 2, label: {1: "Resta", 2: "Subtraction"}, img: "assets/img/2.png"},
    {id: 3, label: {1: "Multiplicación", 2: "Multiplication"}, img: "assets/img/3.png"},
    {id: 4, label: {1: "División", 2: "Division"}, img: "assets/img/4.png"},
  ];


  mensajes: any = {
    dificultad: {1: "Nivel", 2: "Level"},
    tiempo: {1: "Tiempo", 2: "Time"},
    dosJugadores: {1: "Dos Jugadores", 2: "Two Players"},
  };


  constructor(private storage: Storage) { }

  ngOnInit() {

    // Configuración del juego
    this.storage.get('conf').then((val) => {
      if (val) this.conf = val;
      else this.storage.set('conf', this.conf); 
    });

    

  }

  

  guardarConf($event){
    this.storage.set('conf', this.conf); 
  }

  cambiarTema(){
    this.conf.temaOscuro = !this.conf.temaOscuro;
    this.storage.set('conf', this.conf); 
  }

  cambiarIdioma(){
    if(this.conf.idioma == 1) this.conf.idioma = 2;
    else this.conf.idioma =1 ;
    this.storage.set('conf', this.conf); 
  }

}
