import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Storage } from '@ionic/storage';
//Vibración
import { Vibration } from "@ionic-native/vibration/ngx";

import { NivelesService } from "../../services/niveles.service";


@Component({
  selector: 'app-dosjugadores',
  templateUrl: './dosjugadores.page.html',
  styleUrls: ['./dosjugadores.page.scss'],
})
export class DosjugadoresPage implements OnInit {

  constructor(
    private activeRoute: ActivatedRoute, private storage: Storage,
    public vibration: Vibration,
    private nivelesService: NivelesService
  ) { }


  pregunta: string;
  correcta: string;
  respuestas: string[] = [];

  historial: boolean[];
  historialPreguntas: string[];

  nivel: number = 0;

  puntuacion1: number = 0;
  vidas1: number = 3;

  puntuacion2: number = 0;
  vidas2: number = 3;

  //Parametros del juego
  segundos: number = 180;
  tiempoString: string = "";
  modo: number;
  dificultad: number;
  titulo: string;
  pregsXnvl: number;
  pregsXnvl_param: number = 0;

  tiempoInicial: number = 183;
  tipoTiempo: number = 0;
  intervalo: any;

  // En el modo principiante, guarda la pregunta anterior
  ansPregunta: string = "";

  rachaActual1: number = 0;
  rachaActual2: number = 0;

  rachaMax1: number = 0;
  rachaMax2: number = 0;

  promXproblema1: number = 0;
  promXproblema2: number = 0;

  tiempoHecho = 0;

  // Banderas
  jugando: boolean = false;
  finIntento: boolean = false;
  nuevoRecord: boolean = false;
  conteo: boolean = false;
  ultimosSegundos: boolean = false;

  // Rangos por nivel
  rangos: any;

  //Audios
  audioCorrect = new Audio('assets/audio/correcto.mp3');
  audioIncorrect = new Audio('assets/audio/incorrecto.mp3');
  audioConteo = new Audio('assets/audio/conteo.mp3');
  audio30Secods = new Audio('assets/audio/30seconds.webm');
  audioFinNormal = new Audio('assets/audio/resultado_normal.mp3');
  audioFinBueno = new Audio('assets/audio/resultado_bueno.mp3');
  audioFinMalo = new Audio('assets/audio/resultado_malo.mp3');


  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: null
  };


  // Checa cuando los dos jugadores esten listos para jugar
  isJugador1Listo: boolean = false;
  isJugador2Listo: boolean = false;

  blockJugador1: boolean = false;
  blockJugador2: boolean = false;

  correctoJugador1: boolean = false;
  correctoJugador2: boolean = false;
  errorJugador1: boolean = false;
  errorJugador2: boolean = false;

  marcador: any = {
    jugadorUno: 0.0,
    jugadorDos: 0.0
  };


  mensajes: any = {
    inicio: { 1: "Comenzar", 2: "Start" },
    espera: { 1: "Esperando al otro jugador", 2: "Waiting the other player" },
    correcto: { 1: "Correcto", 2: "Correct" },
    incorrecto: { 1: "¡Error!", 2: "Error!" },
    ganar: { 1: "¡Has ganado!", 2: "You win!" },
    perder: { 1: "¡Has perdido!", 2: "You lose!" },
    empatar: { 1: "¡Empate!", 2: "Draw!" },
    revancha: { 1: "Revancha", 2: "Rematch" },
    salir: { 1: "Salir", 2: "Exit" },
    puntuacion: { 1: "Tu puntuación", 2: "Score" },
    marcador: { 1: "Marcador", 2: "Match" },
  };

  // Mensaje del final para los jugadores
  msjP1: string = "";
  msjP2: string = "";

  // Resultado final de los jugadores pierden, empatan o ganan
  resP1: number;
  resP2: number;

  ngOnInit() {


    // Configuración del juego
    this.storage.get('conf').then((val) => {
      if (val) this.conf = val;
      else this.storage.set('conf', this.conf);
    });

    this.modo = parseInt(this.activeRoute.snapshot.paramMap.get("modo"));
    this.dificultad = parseInt(this.activeRoute.snapshot.paramMap.get("dificultad"));
    this.tipoTiempo = parseInt(this.activeRoute.snapshot.paramMap.get("tiempo"));



    if (this.tipoTiempo == 0) {
      this.tiempoInicial = 63;
    }
    if (this.tipoTiempo == 1) {
      this.tiempoInicial = 123;
    }
    if (this.tipoTiempo == 2) {
      this.tiempoInicial = 183;
    }

    this.rangos = this.nivelesService.getRangos(this.modo);
    this.pregsXnvl = this.rangos[this.nivel][this.dificultad].cantidad;
  }


  estoyListo(tipo) {

    if (tipo == 1) {
      this.isJugador1Listo = true;

    }
    if (tipo == 2) {
      this.isJugador2Listo = true;
    }

    if (this.isJugador1Listo && this.isJugador2Listo) {
      //Comenzar partida
      this.iniciar();

    }
  }


  controlTiempo() {


    if (this.segundos > 0) {
      this.tiempoHecho += 1;

      this.segundos -= 1;
      this.tiempoString = this.parseTiempo(this.segundos);

      if (this.segundos == this.tiempoInicial - 3) {
        this.conteo = false;
        if (!this.jugando) {
          this.jugando = true;
        } else {
          this.finIntento = false;
        }
      }

      if (this.segundos == 30) {
        this.ultimosSegundos = true;
        this.audio30Secods.currentTime = 0;
        this.audio30Secods.play();

      }
    }
    else {
      this.vibration.vibrate(400);
      this.terminarIntento();
    }

  }

  playEnd() {
    this.audioFinNormal.currentTime = 0;
    this.audioFinNormal.play();
    this.vibration.vibrate(400);
  }

  terminarIntento() {
    this.playEnd();
    this.isJugador1Listo = false;
    this.isJugador2Listo = false;

    clearInterval(this.intervalo);

    if (this.puntuacion1 > this.puntuacion2) {
      this.jugador1Gana();

    }

    if (this.puntuacion2 > this.puntuacion1) {
      this.jugador2Gana();
    }

    if (this.puntuacion1 == this.puntuacion2) {
      this.jugadoresEmpatan();
    }

    this.finIntento = true;


  }



  //Controlador de tipo de pregunta
  crearPregunta() {
    let max = this.rangos[this.nivel][this.dificultad].max;
    let min = this.rangos[this.nivel][this.dificultad].min;


    let aux: number;
    let oprStr = '';
    let resultado: number;
    let modo = this.modo;

    if (this.modo == 5) {
      modo = this.getRandomInt(1, 4);
    }

    if (this.modo == 6) {
      modo = this.getRandomInt(1, 2);
    }

    if (this.modo == 7) {
      modo = this.getRandomInt(3, 4);
    }

    let numero1 = this.getRandomInt(min, max);

    // Evita la division entre 0
    if (modo == 4 && min == 0) {
      min += 1;
    }
    let numero2 = 0;

    // Crea multiplicaciones y divisiones faciles en los niveles más bajos ( < 5)
    if ((modo == 4 || modo == 3) && this.nivel < 5) {
      if (numero1 > max / 2) {
        numero2 = this.getRandomInt(min, max / 2);
      } else {
        numero2 = this.getRandomInt(max / 2, max);

      }
    }
    else {
      numero2 = this.getRandomInt(min, max);

    }




    // Si es nivel principiante (quitar los resultados en restas negativos)
    let auxRes = 0;
    if (this.dificultad == 0) {
      if (modo == 2) {
        if (numero2 > numero1) {
          [numero2, numero1] = [numero1, numero2];
        }
      }

    }



    switch (modo) {
      case 1:
        resultado = numero1 + numero2;
        oprStr = '+';
        break;

      case 2:
        resultado = numero1 - numero2;
        oprStr = '-';
        break
      case 3:
        resultado = numero1 * numero2;
        oprStr = 'x';
        break;

      case 4:
        aux = numero1 * numero2;
        resultado = numero1;
        numero1 = aux;
        oprStr = '/';
        break;
      default:
        break;
    }


    this.pregunta = `${numero1}${oprStr}${numero2}`;

    // Verifica que no se repitan las preguntas
    if (this.historialPreguntas.includes(this.pregunta)) {
      return this.crearPregunta();
    } else {

      if (this.dificultad != 0) {
        this.historialPreguntas.push(this.pregunta);
      } else {
        if (this.ansPregunta === this.pregunta) {

          return this.crearPregunta();
        }
        else {
          this.ansPregunta = this.pregunta;
        }
      }
      this.correcta = (resultado) + '';
      this.respuestas = [];
      // Generar respuestas aleatorias 'inteligentes'
      this.respuestas = this.generarRespuestas(resultado, 2);
      // Mezclar el orden de las respuestas
      this.respuestas = this.shuffle(this.respuestas);

    }


  }




  responder(respuesta: string, jugador: number) {

    if (this.correcta === respuesta) {
      if (jugador == 1) {
        this.puntuacion1 += 1;
        this.blockJugador2 = true;
        this.blockJugador1 = true;
        this.correctoJugador1 = true;

      }
      else {
        this.puntuacion2 += 1;
        this.blockJugador1 = true;
        this.correctoJugador2 = true;
        this.blockJugador2 = true;
      }

    } else {
      if (jugador == 1) {
        this.vidas1 -= 1;
        this.blockJugador1 = true;


        this.errorJugador1 = true;

      } else {
        this.vidas2 -= 1;
        this.blockJugador2 = true;
        this.errorJugador2 = true;

      }

    }


    this.pregsXnvl_param += 1;
    if (this.pregsXnvl_param == this.pregsXnvl) {
      this.pregsXnvl_param = 0;
      this.nivel += 1;
      this.pregsXnvl = this.rangos[this.nivel][this.dificultad].cantidad;

    }

    if (this.vidas1 == 0 || this.vidas2 == 0) {
      if (this.vidas1 == 0) {
        this.jugador2Gana();
      }

      if (this.vidas2 == 0) {
        this.jugador1Gana();
      }
    } else {

      if (this.correctoJugador1 || this.correctoJugador2) {
        if ((this.correctoJugador1 && !this.errorJugador2) || (this.correctoJugador2 && !this.errorJugador1)) {
          setTimeout(() => {
            this.crearPregunta2Jugadores();
          }, 1000);
        } else {
          this.crearPregunta2Jugadores();
        }
      }

      if (this.errorJugador1 && this.errorJugador2) {
        setTimeout(() => {
          this.crearPregunta2Jugadores();
        }, 500);
      }

    }

  }

  jugadoresEmpatan() {
    this.playEnd();
    this.isJugador1Listo = false;
    this.isJugador2Listo = false;
    clearInterval(this.intervalo);


    this.resP1 = 1;
    this.resP2 = 1;

    this.msjP1 = this.mensajes.empatar[this.conf.idioma];
    this.msjP2 = this.mensajes.empatar[this.conf.idioma];
    this.marcador.jugadorUno += 0.5;
    this.marcador.jugadorDos += 0.5;

    this.finIntento = true;
  }


  jugador2Gana() {
    this.playEnd();

    this.isJugador1Listo = false;
    this.isJugador2Listo = false;
    clearInterval(this.intervalo);

    this.resP1 = 0;
    this.resP2 = 1;


    this.msjP1 = this.mensajes.perder[this.conf.idioma];
    this.msjP2 = this.mensajes.ganar[this.conf.idioma];
    this.marcador.jugadorDos += 1;

    this.finIntento = true;
  }

  jugador1Gana() {
    this.playEnd();

    this.isJugador1Listo = false;
    this.isJugador2Listo = false;
    clearInterval(this.intervalo);

    this.resP1 = 1;
    this.resP2 = 0;

    this.msjP2 = this.mensajes.perder[this.conf.idioma];
    this.msjP1 = this.mensajes.ganar[this.conf.idioma];
    this.marcador.jugadorUno += 1;

    this.finIntento = true;
  }

  crearPregunta2Jugadores() {
    this.blockJugador1 = false;
    this.blockJugador2 = false;
    this.correctoJugador1 = false;
    this.correctoJugador2 = false;
    this.errorJugador1 = false;
    this.errorJugador2 = false;
    this.crearPregunta();
  }


  iniciar() {

    this.reiniciarParametros();
    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1000);
    this.conteo = true;
    this.audioConteo.currentTime = 0;
    this.audioConteo.play();
  }

  reintentar() {

    this.reiniciarParametros();
    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1000);
    this.conteo = true;
    this.audioConteo.currentTime = 0;
    this.audioConteo.play();
  }

  reiniciarParametros() {
    this.segundos = this.tiempoInicial;
    this.tiempoHecho = 0;
    this.ultimosSegundos = false;
    this.tiempoString = this.parseTiempo(this.segundos);
    this.historialPreguntas = [];
    this.historial = [];
    this.nivel = 0;
    this.vidas1 = 3;
    this.puntuacion1 = 0;

    this.vidas2 = 3;
    this.puntuacion2 = 0;

    this.rachaActual1 = 0;
    this.blockJugador1 = false;
    this.blockJugador2 = false;
    this.correctoJugador1 = false;
    this.correctoJugador2 = false;
    this.errorJugador1 = false;
    this.errorJugador2 = false;

    this.crearPregunta();
  }


  // Creador de respuestas
  generarRespuestas(base: string | number, cantidad: number) {
    //Generar 'cantidad' de respuestas
    let respuestas = [];
    while (respuestas.length < cantidad) {
      //Tipos de respuesta Random
      let tipo = parseInt((Math.random() * 101) + '');
      let propuesta = base;
      let baseStr = base + '';
      let n = baseStr.length

      //Suffle
      if ((tipo >= 0 && tipo < 25) && baseStr.length > 1) {
        propuesta = parseInt(baseStr.split('').sort(function () {
          return 0.5 - Math.random()
        }).join(''));
      }

      //Acercamiento a la respuesta en un intevalo bajo
      if (tipo >= 25 && tipo <= 60) {
        let porcentaje = parseInt((parseInt(base + '') / 10) + '')
        propuesta = parseInt(base + '') + this.randomInRange(-porcentaje, porcentaje)
      }

      //Diferencias de 10*size 
      if (tipo > 60 && tipo <= 100) {
        let numero = this.randomInRange(-n, n);
        propuesta = parseInt(base + '') + (numero * 10)

      }


      if (propuesta != base && !respuestas.includes(propuesta + '')) respuestas.push(propuesta + '');

    }
    respuestas.push(base + '');
    return respuestas;
  }




  // Funciones auxiliares
  parseTiempo(segundos: number) {

    let minString: string = (segundos / 60) + "";
    let min = parseInt(minString);
    let seg = segundos % 60;

    let resultado = "";

    if (seg < 10) {
      resultado = `${min}:0${seg}`;
    } else {
      resultado = `${min}:${seg}`;
    }

    return resultado;

  }

  shuffle(a: any[] | string[]) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  randomInRange(min: string | number, max: string | number) {
    min = parseInt(min + '');
    max = parseInt(max + '');
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }


  toArray(cantidad: number) {
    var arreglo = [];
    for (let i = 0; i < cantidad; i++) {
      arreglo.push("");
    }
    return arreglo;
  }

  getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  ionViewWillLeave() {
    if (this.jugando && !this.finIntento) {
      this.terminarIntento();
    }
  }

}



