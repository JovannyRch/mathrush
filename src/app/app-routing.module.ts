import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'menu1', loadChildren: './pages/menu1/menu1.module#Menu1PageModule' },
  { path: 'juego/:modo/:dificultad/:tiempo', loadChildren: './pages/juego/juego.module#JuegoPageModule' },
  { path: 'estadisticas', loadChildren: './pages/estadisticas/estadisticas.module#EstadisticasPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'dosjugadores/:modo/:dificultad/:tiempo', loadChildren: './pages/dosjugadores/dosjugadores.module#DosjugadoresPageModule' },
  { path: 'language', loadChildren: './pages/language/language.module#LanguagePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
