import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Router } from "@angular/router";


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: null
  };

  icon: string = "";

  constructor(private storage: Storage, private router: Router) {}
  ngOnInit(): void {

      let opcIcon = parseInt((Math.random()*2)+"");
      
      if(opcIcon == 0) this.icon = "rocket";
      if(opcIcon == 1) this.icon = "planet";

     // Configuración del juego
     this.storage.get('conf').then((val) => {
      if (val) this.conf = val;
    });
  }

  iniciar(){
    if(!this.conf.idioma){
      this.router.navigate(["/language"]);
    }else {
      this.router.navigate(["/menu1"])
    }
  }
}
